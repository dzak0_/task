package com.task.expression.evaluator;

import com.fathzer.soft.javaluator.DoubleEvaluator;

public class DoubleExpressionEvaluatorImpl implements ExpressionEvaluator {

    private final DoubleEvaluator doubleEvaluator;

    public DoubleExpressionEvaluatorImpl(DoubleEvaluator doubleEvaluator) {
        this.doubleEvaluator = doubleEvaluator;
    }

    @Override
    public Double evaluateExpression(String expression) {
        return doubleEvaluator.evaluate(expression);
    }
}
