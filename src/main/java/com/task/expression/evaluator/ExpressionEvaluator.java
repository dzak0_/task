package com.task.expression.evaluator;

public interface ExpressionEvaluator {

    Double evaluateExpression(String expression);
}
