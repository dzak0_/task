package com.task.expression.generator;

public interface ExpressionGenerator {

    String generateExpression();
}
