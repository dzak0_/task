package com.task.properties;

public final class Properties {

    public static final String QUEUE_CAPACITY_PROPERTY_NAME = "queue.capacity";
    public static final int QUEUE_CAPACITY = Integer.parseInt(System.getProperty(QUEUE_CAPACITY_PROPERTY_NAME));

    private Properties() {
    }

}
