package com.task.runner;

import com.task.expression.evaluator.ExpressionEvaluator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.concurrent.BlockingQueue;

public class Consumer implements Runnable {

    private final BlockingQueue<String> queue;
    private final ExpressionEvaluator calculator;
    private static final Logger logger = LogManager.getLogger(Consumer.class);

    public Consumer(BlockingQueue<String> queue, ExpressionEvaluator calculator) {
        this.queue = queue;
        this.calculator = calculator;
    }

    @Override
    public void run() {
            while (!queue.isEmpty()) {
                try {
                    Double calculatedValue = calculator.evaluateExpression(queue.take());
                    logger.info("Calculated value: {}", calculatedValue);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

        }

    }


