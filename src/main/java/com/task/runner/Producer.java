package com.task.runner;

import com.task.expression.generator.ExpressionGenerator;
import com.task.properties.Properties;

import java.util.concurrent.BlockingQueue;

public class Producer implements Runnable {

    private final BlockingQueue<String> queue;
    private final ExpressionGenerator expressionGenerator;

    public Producer(BlockingQueue<String> queue, ExpressionGenerator expressionGenerator) {
        this.queue = queue;
        this.expressionGenerator = expressionGenerator;
    }

    @Override
    public void run() {
        while (!isQueueFull() && isQueueSizeBiggerThanHalfCapacity() || queue.isEmpty()) {
            if (queue.isEmpty()) {
                for (int i = 0; i < Properties.QUEUE_CAPACITY; i++) {
                    offerExpression();
                }
                break;
            }
            offerExpression();
        }

    }

    private void offerExpression() {
        queue.offer(expressionGenerator.generateExpression());
    }

    private boolean isQueueSizeBiggerThanHalfCapacity() {
        return queue.size() >= Properties.QUEUE_CAPACITY / 2;
    }

    private boolean isQueueFull() {
        return queue.size() >= Properties.QUEUE_CAPACITY;
    }
}
