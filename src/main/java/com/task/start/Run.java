package com.task.start;

import com.fathzer.soft.javaluator.DoubleEvaluator;
import com.task.expression.evaluator.DoubleExpressionEvaluatorImpl;
import com.task.expression.evaluator.ExpressionEvaluator;
import com.task.expression.generator.ExpressionGenerator;
import com.task.expression.generator.ExpressionGeneratorImpl;
import com.task.properties.Properties;
import com.task.runner.Consumer;
import com.task.runner.Producer;

import java.util.concurrent.*;
public class Run {

    public static void main(String[] args) {
        BlockingQueue<String> queue = new LinkedBlockingDeque<>(Properties.QUEUE_CAPACITY);
        ExpressionGenerator expressionGenerator = new ExpressionGeneratorImpl();
        DoubleEvaluator doubleEvaluator = new DoubleEvaluator();
        ExpressionEvaluator expressionEvaluator = new DoubleExpressionEvaluatorImpl(doubleEvaluator);

        startProducers(queue, expressionGenerator);
        startConsumers(queue, expressionEvaluator);


    }

    private static void startConsumers(BlockingQueue<String> queue, ExpressionEvaluator expressionEvaluator) {
        Consumer consumer1 = new Consumer(queue, expressionEvaluator);
        Consumer consumer2 = new Consumer(queue, expressionEvaluator);
        Consumer consumer3 = new Consumer(queue, expressionEvaluator);
        Consumer consumer4 = new Consumer(queue, expressionEvaluator);

        ScheduledExecutorService consumerExecutorService = Executors.newScheduledThreadPool(4);
        consumerExecutorService.scheduleAtFixedRate(consumer1, 0, 3, TimeUnit.MILLISECONDS);
        consumerExecutorService.scheduleAtFixedRate(consumer2, 0, 3, TimeUnit.MILLISECONDS);
        consumerExecutorService.scheduleAtFixedRate(consumer3, 0, 3, TimeUnit.MILLISECONDS);
        consumerExecutorService.scheduleAtFixedRate(consumer4, 0, 3, TimeUnit.MILLISECONDS);
    }

    private static void startProducers(BlockingQueue<String> queue, ExpressionGenerator expressionGenerator) {
        Producer taskProducer1 = new Producer(queue, expressionGenerator);
        Producer taskProducer2 = new Producer(queue, expressionGenerator);

        ScheduledExecutorService producer = Executors.newScheduledThreadPool(2);
        producer.scheduleAtFixedRate(taskProducer1, 0, 3, TimeUnit.MILLISECONDS);
        producer.scheduleAtFixedRate(taskProducer2, 0, 3, TimeUnit.MILLISECONDS);
    }
}
