package expression.evaluator;

import com.fathzer.soft.javaluator.DoubleEvaluator;
import com.task.expression.evaluator.DoubleExpressionEvaluatorImpl;
import com.task.expression.evaluator.ExpressionEvaluator;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;


class ExpressionEvaluatorTest {


    @Test
    void shouldReturnValidResult() {
        //given
        ExpressionEvaluator expressionEvaluator = new DoubleExpressionEvaluatorImpl(new DoubleEvaluator());
        //when
        Double result = expressionEvaluator.evaluateExpression("32+2");
        //then
        Assertions.assertEquals(34, result.doubleValue());
    }

}