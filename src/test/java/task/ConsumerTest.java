package task;

import com.fathzer.soft.javaluator.DoubleEvaluator;
import com.task.runner.Consumer;
import com.task.expression.evaluator.DoubleExpressionEvaluatorImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import com.task.properties.Properties;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

class ConsumerTest {

    private static final int QUEUE_CAPACITY = 10;
    private static final String ONE_EXPRESSION = "2+2";
    private static BlockingQueue<String> queue;
    private static Consumer consumer;

    @BeforeAll
    static void init() {
        System.setProperty(Properties.QUEUE_CAPACITY_PROPERTY_NAME, String.valueOf(QUEUE_CAPACITY));
    }

    @BeforeEach
    void initCreate() {
        queue = new LinkedBlockingQueue<>(QUEUE_CAPACITY);
        consumer = new Consumer(queue, new DoubleExpressionEvaluatorImpl(new DoubleEvaluator()));
    }


    @Test
    void shouldBeAbleToConsumeElementFromQueue() {
        queue.add(ONE_EXPRESSION);
        int initSize = queue.size();
        consumer.run();
        Assertions.assertEquals(1, initSize);
        Assertions.assertTrue(queue.isEmpty());
    }

    @Test
    void shouldFinishGracefullyWhenQueueIsEmpty() {
        consumer.run();
        Assertions.assertTrue(queue.isEmpty());
    }

}