package task;

import com.task.runner.Producer;
import com.task.expression.generator.ExpressionGeneratorImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import com.task.properties.Properties;

import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

class ProducerTest {

    private static final int QUEUE_CAPACITY = 10;
    public static final List<String> FIVE_DUMMY_EXPRESSIONS = List.of("2+0", "2+1", "2+2", "2+3", "2+4");
    public static final List<String> TEN_DUMMY_EXPRESSIONS = List.of(
            "2+0", "2+1", "2+2",
            "2+3", "2+4","1+1",
            "2+2","4*3","2*6",
            "2-1"
    );
    private static BlockingQueue<String> queue;
    private static Producer producer;

    @BeforeAll
    static void init() {
        System.setProperty(Properties.QUEUE_CAPACITY_PROPERTY_NAME, String.valueOf(QUEUE_CAPACITY));
    }

    @BeforeEach
    void initCreate() {
        queue = new LinkedBlockingQueue<>(QUEUE_CAPACITY);
        producer = new Producer(queue, new ExpressionGeneratorImpl());
    }


    @Test
    void shouldBeAbleToAddElementsWhenQueueIsEmpty() {
        //when
        producer.run();
        //then
        Assertions.assertEquals(QUEUE_CAPACITY, queue.size());
    }

    @Test
    void shouldBeAbleToAddElementsWhenQueueIsHalfEmpty() {
        //given
        queue.addAll(FIVE_DUMMY_EXPRESSIONS);
        //when
        producer.run();
        //then
        Assertions.assertEquals(QUEUE_CAPACITY, queue.size());
    }

    @Test
    void shouldNotBeAbleToAddElementWhenQueueIsFull() {
        //given
        queue.addAll(TEN_DUMMY_EXPRESSIONS);
        //when
        producer.run();
        //then
        Assertions.assertEquals(QUEUE_CAPACITY, queue.size());
    }

}